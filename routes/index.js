const router = require("express").Router();
const room = require("./room");
const movie = require("./movie");
const { check } = require("../middlewares/check")

router.use("/room", room);
router.use("/movie", check, movie);
router.use("/submit-name", (req, res) => {
  req.session.name = req.body.name;
  res.redirect("/movie");
});
router.use("/", check, (req, res) => {
  res.render("pages/index");
});

module.exports = router;

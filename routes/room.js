const router = require("express").Router();
const roomController = require("../controllers/room");

router.get("/", roomController.enterRoom);
router.post("/create", roomController.createRoom);
router.post("/join", roomController.joinRoom);
router.get("/leave", roomController.leaveRoom);

module.exports = router;

const router = require("express").Router();
const movieController = require("../controllers/movie");

router.get("/", movieController.getMovies);

module.exports = router;

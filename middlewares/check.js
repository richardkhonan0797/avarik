// If user has joined a movie before, redirect to room
const check = (req, res, next) => {
  if (typeof req.session.movie_id !== "undefined") {
    res.redirect("/room");
  } else {
    next();
  }
};

module.exports = { check };

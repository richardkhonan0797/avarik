## Content
- [1. Installation](#1-installation)
  - [Requirements](#requirements)
  - [How to Install](#how-to-install)
- [2. How to Use](#2-how-to-use)
  - [Features](#features)
  - [Step to Use](#step-to-use)
- [3. Infrastructure Diagram](#3-infrastructure-diagram)
- [4. Framework and Library Used](#4-framework-and-library-used)

# 1. Installation
## Requirements:
  - Operating System \(Linux, Windows with WSL\)
  - Docker

## How to Install
   1.  _git clone_ [https://gitlab.com/richardkhonan0797/avarik.git](https://gitlab.com/richardkhonan0797/avarik.git)
   2. Get into avarik folder
   3. When running for the first time _docker compose up --build_, for next run and beyond _docker compose up_

# 2. How to Use

## Features

1. Input Name
2. List of movies to select from
3. Create Room
4. Join Room (with code)
5. In the room (the host and guest) can:
    * Play, Pause, Fast Forward, Backward the Video (all sync across users in the
same room)
    * Simple Chat mechanism among user in the room
6. Simple UI

## Steps
1. Start with _docker compose up_ command
2. Access _localhost:3000_
3. Input your name to proceed to movie selection page  
   ![](/public/images/1.png)
4. Click on a movie to create room  
   ![](/public/images/2.png)
5. Input code and click join button to join an existing room  
   ![](/public/images/3.png)
6. Users can chat  
   ![](/public/images/4.png)
7. Click _Leave Room_ button to leave  
   ![](/public/images/5.png)
8. Redirected to index page after leaving room  
   ![](/public/images/1.png)

# 3. Infrastructure Diagram
   ![](/public/images/6.png)

# 4. Framework and Library Used
- [Expressjs](https://expressjs.com/): A NodeJS framework to help me create the server.
- [Socket.IO](https://socket.io/): Socket.IO is a library that enables bidirectional communication between a client and a server. And in this project Socket.IO is used to sync video data and chat between the clients.
- [EJS](https://ejs.co/): A templating engine and it helps in rendering dynamic data easier.

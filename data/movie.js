const movies = [
  {
    id: 1,
    movie_name: "Movie 1",
    movie_url:
      "/video/Big_Buck_Bunny_4K.webm.720p.webm",
  },
  {
    id: 2,
    movie_name: "Movie 2",
    movie_url:
      "/video/Big_Buck_Bunny_4K.webm.720p.webm",
  },
  {
    id: 3,
    movie_name: "Movie 3",
    movie_url:
      "/video/Big_Buck_Bunny_4K.webm.720p.webm",
  },
];

module.exports = {
    movies
}
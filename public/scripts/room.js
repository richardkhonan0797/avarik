let socket = io("");

let movie_id = $("#room").data("movieid");
let name = $("#room").data("name");
let host = $("#room").data("host");
let time = $("#room").data("time");
let code = $("#room").data("code");
let video = document.getElementById("video");
let form = document.getElementById("form-chat");
let input = document.getElementById("input-chat");

// Buttons
var playButton = document.getElementById("play-pause");
var muteButton = document.getElementById("mute");
var fullScreenButton = document.getElementById("full-screen");

// Sliders
var seekBar = document.getElementById("seek-bar");
var volumeBar = document.getElementById("volume-bar");

video.currentTime = time;

// Event listener for emitting new chat message
form.addEventListener("submit", function (e) {
  e.preventDefault();
  if (input.value) {
    socket.emit("chat message", { message: input.value, name });
    input.value = "";
  }
});

// Event listener for emitting play and pause and update button text
playButton.addEventListener("click", function () {
  if (video.paused == true) {
    video.play();
    socket.emit("onplay", {
      time: video.currentTime,
    });
    playButton.innerHTML = "Pause";
  } else {
    video.pause();
    socket.emit("onpause");

    playButton.innerHTML = "Play";
  }
});

// Calculate new time and emit to another clients
seekBar.addEventListener("change", function () {
  // Calculate the new time
  var time = video.duration * (seekBar.value / 100);

  // Update the video time
  video.currentTime = time;
  socket.emit("onseeked", {
    time: video.currentTime,
  });
});

// Update volume and emit to another clients
volumeBar.addEventListener("change", function () {
  // Update the video volume
  video.volume = volumeBar.value;
  socket.emit("onvolumechange", {
    volume: video.volume,
  });
});

// listen ontimeupdate event to emit video time and update seek bar value
video.ontimeupdate = () => {
  if (host) {
    socket.emit("update video time", {
      time: video.currentTime,
    });
  }

  // Calculate the slider value
  var value = (100 / video.duration) * video.currentTime;

  // Update the slider value
  seekBar.value = value;
};

// emit needed data for joining room
socket.emit("join room", {
  name,
  movie_id,
  code,
  host,
});

// update user list when there is a change
socket.on("user list", (data) => {
  $("#user-list").empty();
  for (let key in data.users) {
    if (data.users[key].host) {
      $("#user-list").append(`<li>${data.users[key].name}<b> (HOST)</b></li>`);
    } else {
      $("#user-list").append(`<li>${data.users[key].name}`);
    }
  }
});

socket.on("play video", (data) => {
  video.currentTime = data.time;
  video.play();
  playButton.innerHTML = "Pause";
});

socket.on("pause video", () => {
  video.pause();
  playButton.innerHTML = "Play";
});

socket.on("volume change", (data) => {
  video.volume = data.volume;
  volumeBar.value = video.volume;
});

socket.on("time change", (data) => {
  video.currentTime = data.time;
});

socket.on("broadcast message", (data) => {
  $("#messages").append(`<li><b>${data.name}</b>: ${data.message}<li>`);
  $("#messages").scrollTop($("#messages").prop("scrollHeight"));
});

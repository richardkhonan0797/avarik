let socket = io();

let movie_id = $(".movie").data("movieid");
let name = $(".movie").data("name");

/*
If the room is just created emit "create room" event
to get the new room code and create new socket room then 
reload to render the "pages/room.ejs" else just reload 
to render "pages/room.ejs"
*/
socket.on("room code", (data) => {
  const room_data = {
    movie_id,
    name,
    code: data.code,
  };
  if (data.create) {
    socket.emit("create room", room_data);
    location.reload();
  } else {
    location.reload();
  }
});

$(function () {
  $(".movie").on("click", (e) => {
    $.ajax({
      url: "/room/create",
      type: "POST",
      data: {
        movie_id,
      },
    });
  });

  $("#join").on("click", (e) => {
    $.ajax({
      url: "/room/join",
      type: "POST",
      data: {
        movie_id,
        code: document.getElementById("code").value,
      },
      error: (err) => {
        alert(err.responseText);
        document.getElementById("code").value = "";
      },
    });
  });
});

const express = require("express");
const app = express();
const port = 3000;

const path = require("path");
const server = require("http").createServer(app);
const bodyParser = require("body-parser");
const session = require("express-session");
const io = require("socket.io")(server);

const router = require("./routes/index");
const users = require("./data/user");
const rooms = require("./data/room");

app.set("views", path.join(__dirname, "views"));
app.set("view engine", "ejs");
app.use(express.static(__dirname + "/public"));
app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());
app.use(
  session({
    secret: "secret",
    resave: false,
    saveUninitialized: true,
    cookie: {},
  })
);
app.use("/", router);

io.on("connection", (socket) => {
  app.set("socket", socket);
  socket.on("disconnect", () => {
    if (typeof rooms[users[socket.id]] !== "undefined") {
      // remove user from room
      delete rooms[users[socket.id]].users[socket.id];
      io.to(users[socket.id]).emit("user list", {
        users: rooms[users[socket.id]].users
          ? rooms[users[socket.id]].users
          : {},
      });
    }
  });

  socket.on("chat message", (data) => {
    io.to(users[socket.id]).emit("broadcast message", {
      name: data.name,
      message: data.message
    })
  });

  socket.on("create room", (data) => {
    rooms[data.code] = {
      id: data.movie_id,
      users: {},
      time: 0,
    };
  });

  socket.on("join room", (data) => {
    // create new user in room
    rooms[data.code].users[socket.id] = {
      name: data.name,
      host: data.host,
    };
    users[socket.id] = data.code;
    socket.join(data.code);
    io.to(data.code).emit("user list", { users: rooms[data.code].users });
  });

  socket.on("update video time", (data) => {
    rooms[users[socket.id]].time = data.time;
  });

  socket.on("onplay", (data) => {
    io.in(users[socket.id]).emit("play video", {
      time: data.time,
    });
  });

  socket.on("onpause", (data) => {
    socket.to(users[socket.id]).emit("pause video");
  });

  socket.on("onvolumechange", (data) => {
    socket.to(users[socket.id]).emit("volume change", {
      volume: data.volume,
    });
  });

  socket.on("onseeked", (data) => {
    socket.to(users[socket.id]).emit("time change", {
      time: data.time,
    });
  });
});

server.listen(port, () => {
  console.log(`Listening on port ${port}`);
});

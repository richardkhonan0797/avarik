const movieData = require("../data/movie");
const roomData = require("../data/room");
const util = require("../helpers/util");

const Room = {
  async enterRoom(req, res) {
    for (let movie of movieData.movies) {
      if (movie.id == req.session.movie_id) {
        return res.render("pages/room", {
          movie_url: movie.movie_url,
          movie_id: movie.id,
          name: req.session.name,
          code: req.session.code,
          host: req.session.host,
          time: roomData[req.session.code].time
            ? roomData[req.session.code].time
            : 0,
        });
      }
    }
  },
  async createRoom(req, res) {
    // generate new room code
    const code = util.generateCode();
    // emit new room code needed for socket room creation
    const socket = req.app.get("socket");
    socket.emit("room code", {
      code,
      create: true,
    });
    req.session.code = code;
    req.session.movie_id = req.body.movie_id;
    req.session.host = true;
    res.redirect("/room");
  },
  async joinRoom(req, res) {
    const code = req.body.code;
    const socket = req.app.get("socket");
    if (!roomData[code]) {
      res.status(400).send("Room not found!");
    } else {
      socket.emit("room code", {
        code,
        create: false,
      });
      req.session.code = code;
      req.session.movie_id = req.body.movie_id;
      req.session.host = false;
      res.redirect("/room");
    }
  },
  async leaveRoom(req, res) {
    req.session.destroy(function () {
      res.redirect("/");
    });
  },
};

module.exports = Room;

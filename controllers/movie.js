const movieData = require("../data/movie");

const Movie = {
  async getMovies(req, res) {
    res.render("pages/movie", {
      movies: movieData.movies,
      name: req.session.name,
    });
  },
};

module.exports = Movie;
